package org.authservice.common.exceptions;

public class PayloadDataException extends RuntimeException {

    public PayloadDataException (String message) {
        super(message, new RuntimeException());
    }

}
