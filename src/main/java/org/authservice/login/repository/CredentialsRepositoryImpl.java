package org.authservice.login.repository;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.authservice.login.model.UserCredentials;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
@Qualifier("credentials")
public class CredentialsRepositoryImpl {
    private final MongoTemplate template;

    public CredentialsRepositoryImpl(MongoTemplate template) {
        this.template = template;
    }

    public Optional<UserCredentials> findOne(Query query) {
        var result = template.findOne(query, UserCredentials.class);
        return Optional.ofNullable(result);
    }

    public Optional<UserCredentials> save(UserCredentials userCredentials) {
        var result = template.save(userCredentials);
        return Optional.of(result);
    }

    public UpdateResult updateFirst(Query query, Update update, Class clazz) {
        return template.updateFirst(query, update, clazz);
    }

    public DeleteResult remove(Query query, Object object) {
        return template.remove(query, object.getClass());
    }

}
