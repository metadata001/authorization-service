package org.authservice.common.exceptions;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.authservice.common.exceptions.handler.InvalidPasswordException;
import org.authservice.login.dto.RemoveUserCredentialsRequest;
import org.authservice.login.dto.UpdateUserNameRequest;
import org.authservice.login.dto.UpdateUserPasswordRequest;
import org.authservice.login.model.UserCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StaticExceptionHandlers {
    private static final Logger logger = LoggerFactory.getLogger(StaticExceptionHandlers.class);

    public static void checkUserNameValid(String username) throws InvalidUserNameException {
        if(username == null || username.length() == 0) {
            var message = "username is missing for request";
            logger.warn(message);
            throw new InvalidUserNameException(message);
        }
    }

    public static void checkCredentialsFound(UserCredentials userCredentials) throws InvalidUserNameException {
        if(userCredentials == null) {
            var message = "credentials were not found for request";
            logger.warn(message);
            throw new CredsNotFoundException(message);
        }
    }

    public static void checkCredentialsFound(UpdateResult result, UpdateUserNameRequest request) throws CredsNotFoundException {
        if(result.getMatchedCount() == 0L) {
            var message = String.format("username not updated for request %n %s", request.getCurrentUsername());
            logger.warn(message);
            throw new CredsNotFoundException(message);
        }
    }

    public static void checkCredentialsFound(UpdateResult result, UpdateUserPasswordRequest request) throws CredsNotFoundException {
        if(result.getMatchedCount() == 0L) {
            var message = String.format("password not updated for request %n %s", request.getUsername());
            logger.warn(message);
            throw new CredsNotFoundException(message);
        }
    }

    public static void checkCredentialsFound(DeleteResult result, RemoveUserCredentialsRequest request) throws CredsNotFoundException {
        if(result.getDeletedCount() == 0L) {
            String message = String.format("credentials were not removed for request %n %s", request.getUsername());
            logger.warn(message);
            throw new CredsNotFoundException(message);
        }
    }

    public static void checkPasswordValid(boolean hashCheck) throws InvalidPasswordException {
        if(!hashCheck) {
            var message = "password failed validation";
            logger.warn(message);
            throw new InvalidPasswordException(message);
        }
    }

}
