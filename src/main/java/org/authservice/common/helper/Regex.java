package org.authservice.common.helper;

public class Regex {
    public static final String LOWERCASE = ".*[a-z]+.+";
    public static final String UPPERCASE = ".*[A-Z]+.*";
    public static final String DIGIT = ".*[0-9]+.*";
    public static final String SPECIAL_CHAR = ".*[@#$&!+=-]+.*";
    public static final String EMAIL = "^[-a-z0-9~!$%^&*_=+}{\\'?]+(\\.[-a-z0-9~!$%^&*_=+}{\\'?]+)*@([a-z0-9_][-a-z0-9_]*(\\.[-a-z0-9_]+)*\\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}))(:[0-9]{1,5})?$";

    private Regex() {

    }
}
