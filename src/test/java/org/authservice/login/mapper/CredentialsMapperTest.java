package org.authservice.login.mapper;

import org.assertj.core.api.Assertions;
import org.authservice.login.dto.UserCredentialsRequest;
import org.authservice.login.model.UserCredentials;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import java.util.List;

class CredentialsMapperTest {
    private static CredentialsMapper mapper;
    private static final String ID = "id";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    @BeforeAll
    public static void setup() {
        mapper = Mappers.getMapper(CredentialsMapper.class);
    }

    @Test
    void mapperShould_mapCreateUserCredentialsRequest_toUserCredentials() {
        var request = someCreateUserCredentialsRequest();
        var userCredentials = mapper.map(request);

        Assertions.assertThat(someUserCredentials())
                .usingRecursiveComparison()
                .ignoringFields(ID)
                .isEqualTo(userCredentials);

    }

    private UserCredentials someUserCredentials() {
        var userCredentials = new UserCredentials();
        return userCredentials.setId(ID)
                .setUsername(USERNAME)
                .setHashedPassword(PASSWORD)
                .setAuthorities(List.of());
    }

    private UserCredentialsRequest someCreateUserCredentialsRequest() {
        var request = new UserCredentialsRequest();
        return request.setUsername(USERNAME)
                .setPassword(PASSWORD)
                .setAuthorities(List.of());
    }

}
