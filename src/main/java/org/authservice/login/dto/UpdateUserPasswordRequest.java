package org.authservice.login.dto;

public class UpdateUserPasswordRequest {
    private String username;
    private String currentPassword;
    private String newPassword;

    public String getUsername() {
        return username;
    }

    public UpdateUserPasswordRequest setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public UpdateUserPasswordRequest setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
        return this;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public UpdateUserPasswordRequest setNewPassword(String password) {
        this.newPassword = password;
        return this;
    }

}
