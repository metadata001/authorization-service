package org.authservice.common.exceptions;

public class InvalidUserNameException extends RuntimeException {

    public InvalidUserNameException(String message) {
        super(message, new RuntimeException());
    }

}
