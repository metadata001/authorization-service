package org.authservice.common.exceptions.handler;

public class InvalidPasswordException extends RuntimeException {

    public InvalidPasswordException(String message) {
        super(message, new RuntimeException());
    }

}
