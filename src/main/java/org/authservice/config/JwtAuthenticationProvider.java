package org.authservice.config;

import org.authservice.jwt.TokenValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;

public class JwtAuthenticationProvider implements AuthenticationProvider {
    Logger logger = LoggerFactory.getLogger(JwtAuthenticationProvider.class);
    private UserDetailsService userDetailsService;
    private final TokenValidationService tokenValidationService;

    public JwtAuthenticationProvider(TokenValidationService tokenValidationService) {
        this.tokenValidationService = tokenValidationService;
    }

    @Override
    public JwtAuthToken authenticate(Authentication authentication) throws AuthenticationException {
        if (!(authentication instanceof JwtAuthToken auth)) {
            return null;
        }

        var username = auth.getPrincipal();
        var tokenString = auth.getCredentials();
        JwtAuthToken newAuthToken = null;

        var details = userDetailsService.loadUserByUsername(username);
        var userValid = details.getUsername().equals(username);

        var validated = tokenValidationService.validateToken(tokenString);
        //stores the compromised user information with the token
        if (!validated.isValid()) {
            var message = String.format("token for user: %s is invalid, details for user has been stored", username);
            logger.warn(message);

            newAuthToken = new JwtAuthToken(username, tokenString);
            newAuthToken.setDetails(details);
            newAuthToken.setAuthenticated(false);

        }
        //checks if token expired, if expired refreshes token and adds token to response header
        else if (validated.isExpired() && userValid) {
            var newToken = tokenValidationService.issueToken(username);
            newAuthToken = new JwtAuthToken(username, newToken.getTokenString());
            newAuthToken.setDetails(details);
            newAuthToken.setAuthenticated(true);
            logger.warn("token was expired, token refresh successful");

        }
        //validates token and adds token to response header if valid
        else if (validated.isValid()  && userValid) {
            newAuthToken = new JwtAuthToken(username, tokenString);
            newAuthToken.setDetails(details);
            newAuthToken.setAuthenticated(true);

        }
        //throws runtime exception with message if the above conditions do not apply
        else {
            throw new RuntimeException("response could not process because token could not be processed");

        }

        return newAuthToken;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return JwtAuthToken.class.isAssignableFrom(authentication);
    }

    public void setUserDetailService(UserDetailsService userDetailService) {
        this.userDetailsService = userDetailService;
    }

    public UserDetailsService getUserDetailsService() {
        return this.userDetailsService;
    }

}
