package org.authservice.login.service;

import org.assertj.core.api.Assertions;
import org.authservice.jwt.model.CompromisedUser;
import org.authservice.jwt.model.CompromisedUserDetails;
import org.authservice.jwt.repository.CompromisedRepositoryImpl;
import org.authservice.jwt.CompromisedTokenService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.List;
import java.util.Optional;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

class CompromisedTokenServiceTest {
    private static final String USERNAME = "username";
    private static final String TOKEN = "token";
    @Mock
    private CompromisedRepositoryImpl compromisedRepository;
    @InjectMocks
    private CompromisedTokenService compromisedTokenService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void storeCompromisedTokenDetails_shouldReturn_compromisedUserIfUserExists() {
        doReturn(Optional.of(mockCompromisedUser())).when(compromisedRepository).findOne(any());
        when(compromisedRepository.save(any())).thenAnswer(arg -> Optional.of(arg.getArguments()[0]));

        var result = compromisedTokenService
                .storeCompromisedTokenDetails(USERNAME, TOKEN);

        Assertions.assertThat(result)
                .usingRecursiveComparison()
                .ignoringFields("details.whenCompromised")
                .isEqualTo(mockCompromisedUserWithDetails());

    }

    @Test
    void storeCompromisedTokenDetails_shouldReturn_compromisedUserIfUserDoesNotExist() {
        when(compromisedRepository.save(any())).thenAnswer(arg -> Optional.of(arg.getArguments()[0]));

        var result = compromisedTokenService
                .storeCompromisedTokenDetails(USERNAME, TOKEN);

        Assertions.assertThat(result)
                .usingRecursiveComparison()
                .ignoringFields("details.whenCompromised")
                .isEqualTo(mockCompromisedUserWithDetails());

    }

    @Test
    void retrieveCompromisedUser_shouldReturn_CompromisedUser() {
        when(compromisedRepository.findOne(any()))
                .thenReturn(Optional.of(mockCompromisedUserWithDetails()));

        var result = compromisedTokenService.retrieveCompromisedUser(USERNAME);

        Assertions.assertThat(result)
                .usingRecursiveComparison()
                .ignoringFields("details.whenCompromised")
                .isEqualTo(mockCompromisedUserWithDetails());
    }

    @Test
    void retrieveCompromisedUsers_shouldReturn_CompromisedUserList() {
        when(compromisedRepository.findAll())
                .thenReturn(Optional.of(List.of(mockCompromisedUserWithDetails())));

        var result = compromisedTokenService.retrieveAllCompromisedUsers();

        Assertions.assertThat(result.get(0))
                .usingRecursiveComparison()
                .ignoringFields("details.whenCompromised")
                .isEqualTo(mockCompromisedUserWithDetails());
    }

    private CompromisedUser mockCompromisedUser() {
        return new CompromisedUser()
                .setUsername(USERNAME);
    }

    private CompromisedUser mockCompromisedUserWithDetails() {
        return mockCompromisedUser()
                .setDetails(List.of(mockCompromisedUserDetails()));
    }

    private CompromisedUserDetails mockCompromisedUserDetails() {
        return new CompromisedUserDetails()
                .setWhenCompromised()
                .setToken(TOKEN);
    }

}
