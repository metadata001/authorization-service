package org.authservice.generate.keys;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import javax.crypto.SecretKey;

class HmacKeyTest {

    private Key key;

    @BeforeEach
    void setup() {
        this.key = new HmacKey();
    }


    @Test
    void generateKey_shouldGenerateAndReturn_keysOfHmacType() {
        var algorithms = this.key.getApprovedAlgorithms();

        var keysMatch = algorithms.stream()
                .map(algo -> this.key.generateKey(algo))
                .map(SecretKey::getAlgorithm)
                .allMatch(algorithms::contains);

        Assertions.assertThat(keysMatch)
                .isTrue();
    }

    @Test
    void getAlgorithm_shouldReturn_jwsAlgorithmImplementations() {
        var algorithms = this.key.getApprovedAlgorithms();

        var algos = algorithms.stream()
                .map(algo -> this.key.getAlgorithm(algo))
                .toList();

        Assertions.assertThat(algos)
                .hasSameSizeAs(this.key.getApprovedJWSAlgorithms());
    }


}
