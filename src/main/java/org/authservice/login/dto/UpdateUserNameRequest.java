package org.authservice.login.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import java.util.ArrayList;
import java.util.List;
import static java.util.stream.Collectors.toCollection;

public class UpdateUserNameRequest {
    private String currentUsername;
    @NotBlank(message = "username is required")
    @Size(min = 6, message = "username must have at least 6 characters")
    private String newUserName;
    private String password;
    private List<String> authorities;

    public String getCurrentUsername() {
        return currentUsername;
    }

    public UpdateUserNameRequest setCurrentUsername(String username) {
        this.currentUsername = username;
        return this;
    }

    public String getNewUserName() {
        return newUserName;
    }

    public UpdateUserNameRequest setNewUserName(String newUserName) {
        this.newUserName = newUserName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UpdateUserNameRequest setPassword(String password) {
        this.password = password;
        return this;
    }

    public List<SimpleGrantedAuthority> getGrantedAuthorities() {
        return checkAuthorities(this.authorities);
    }

    public UpdateUserNameRequest setAuthorities(List<String> authorities) {
        this.authorities = authorities;
        return this;
    }

    private List<SimpleGrantedAuthority> checkAuthorities(List<String> authorities) {
        if(authorities != null && !authorities.isEmpty()) {
            return authorities.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(toCollection(ArrayList<SimpleGrantedAuthority>::new));
        }
        else {
            return new ArrayList<>();
        }
    }

}
