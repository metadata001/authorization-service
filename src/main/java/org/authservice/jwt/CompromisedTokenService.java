package org.authservice.jwt;

import org.authservice.jwt.model.CompromisedUser;
import org.authservice.jwt.model.CompromisedUserDetails;
import org.authservice.jwt.repository.CompromisedRepositoryImpl;
import org.springframework.stereotype.Component;
import java.util.List;
import static org.authservice.login.service.query.CredentialsQuery.getByUserNameQuery;

@Component
public class CompromisedTokenService {
    private final CompromisedRepositoryImpl compromisedRepository;

    public CompromisedTokenService(CompromisedRepositoryImpl compromisedRepository) {
        this.compromisedRepository = compromisedRepository;
    }

    public CompromisedUser storeCompromisedTokenDetails(String username, String tokenString) {
        var query = getByUserNameQuery(username);
        var compromisedUser = compromisedRepository.findOne(query)
                .orElse(new CompromisedUser(null, null));

        if(username != null) {
            var details = new CompromisedUserDetails();
            details
                    .setWhenCompromised()
                    .setToken(tokenString);
            compromisedUser
                    .setUsername(username)
                    .addDetails(details);
            var message = String.format("compromised user: %s , could not be saved", username);

            return compromisedRepository.save(compromisedUser)
                    .orElseThrow(() -> new RuntimeException(message));

        }
        else {
            throw new RuntimeException("compromised user could not be saved, username is null");

        }
    }

    public CompromisedUser retrieveCompromisedUser(String username) {
        return this.compromisedRepository.findOne(getByUserNameQuery(username))
                .orElse(null);
    }

    public List<CompromisedUser> retrieveAllCompromisedUsers() {
        return this.compromisedRepository.findAll()
                .orElse(null);
    }

}
