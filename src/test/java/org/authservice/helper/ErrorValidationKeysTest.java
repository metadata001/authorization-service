package org.authservice.helper;

import org.authservice.common.helper.ErrorValidationKeys;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ErrorValidationKeysTest {

    @Test
    void getTargets_should_not_throw_error() {
        Assertions.assertDoesNotThrow(() -> {
            ErrorValidationKeys.getTargets();
        });
    }

}
