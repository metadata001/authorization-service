package org.authservice.login.service;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.assertj.core.api.Assertions;
import org.authservice.jwt.model.NewTokenResponse;
import org.authservice.jwt.TokenValidationService;
import org.authservice.login.dto.RemoveUserCredentialsRequest;
import org.authservice.login.dto.UpdateUserNameRequest;
import org.authservice.login.dto.UpdateUserPasswordRequest;
import org.authservice.login.dto.UserCredentialsRequest;
import org.authservice.login.mapper.CredentialsMapper;
import org.authservice.login.model.DetailModel;
import org.authservice.login.model.UserCredentials;
import org.authservice.login.repository.CredentialsRepositoryImpl;
import org.bson.BsonString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.util.List;
import java.util.Optional;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class LoginServiceTest {
    private static final String ID = "id";
    private static final String USERNAME = "username";
    private static final String NEW_USERNAME = "new_username";
    private static final String PASSWORD = "password";
    private static final String TOKEN = "token";
    @Mock
    private CredentialsRepositoryImpl repository;
    @Mock
    private CredentialsMapper mapper;
    @Mock
    private BCryptPasswordEncoder encoder;
    @Mock
    private AuthenticationManager authenticationManager;
    @Mock
    private TokenValidationService tokenValidationService;
    @InjectMocks
    private LoginService loginService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void signup_shouldReturn_userCredentials() {
        var mockCredentials = mock(UserCredentials.class);
        when(encoder.encode(anyString())).thenReturn("");
        when(mapper.map(any())).thenReturn(mockCredentials);
        when(repository.findOne(any(Query.class))).thenReturn(Optional.empty());
        when(repository.save(any())).thenReturn(Optional.of(mockCredentials));

        var result = loginService.signup(mock(UserCredentialsRequest.class));

        Assertions.assertThat(result)
                .isEqualTo(mockCredentials);
    }

    @Test
    void login_shouldAuthenticateToken_andReturnDetailModel() {
        var mockDetailModel = mock(DetailModel.class);
        when(tokenValidationService.issueToken(anyString()))
                .thenReturn(mock(NewTokenResponse.class));
        when(authenticationManager.authenticate(any()))
                .thenReturn(mock(Authentication.class));
        when(authenticationManager.authenticate(any()).getPrincipal())
                .thenReturn(mockDetailModel);

        var request = mockUserCredentialsRequest();
        var result = loginService.login(request);

        verify(authenticationManager, times(2)).authenticate(any());

        Assertions.assertThat(result)
                .isEqualTo(mockDetailModel);
    }

    @Test
    void updateUsername_shouldAuthenticateToken_andReturnDetailModel() {
       var mockDetailModel = mockDetailModel();
       mockDetailModel.setUsername(NEW_USERNAME);
       mockDetailModel.setToken(TOKEN);

       doReturn(mockToken()).when(authenticationManager).authenticate(any());
       doReturn(mockUpdateResult()).when(repository).updateFirst(any(), any(), any());
       doReturn(mockNewTokenResponse()).when(tokenValidationService).issueToken(anyString());

       var result = loginService.updateUsername(mockUpdateUserNameRequest());

        verify(authenticationManager, times(2)).authenticate(any());

        Assertions.assertThat(result)
                .usingRecursiveComparison()
                .isEqualTo(mockDetailModel);
    }

    @Test
    void updatePassword_shouldReturn_UpdateResult() {
        var request = mockUpdateUserPasswordRequest()
                .setCurrentPassword(PASSWORD);
        doReturn(Optional.of(mockUserCredentials())).when(repository).findOne(any());
        doReturn(mockToken()).when(authenticationManager).authenticate(any());
        doReturn(PASSWORD).when(encoder).encode(anyString());
        doReturn(mockUpdateResult()).when(repository).updateFirst(any(), any(), any());

        var result = loginService.updatePassword(request);

        Assertions.assertThat(result.getModifiedCount())
                .isEqualTo(1);
    }

    @Test
    void removeCredentials_shouldIndicate_UserDeleted() {
        var request = mockRemoveUserCredentialsRequest();
        var mockCredentials = mockUserCredentials();
        doReturn(Optional.of(mockCredentials)).when(repository).findOne(any());
        when(authenticationManager.authenticate(any())).thenReturn(mock(Authentication.class));
        doReturn(mockDeleteResult()).when(repository).remove(any(), any());

        var result = loginService.removeCredentials(request);

        Assertions.assertThat(result.getDeletedCount())
                .isEqualTo(1);
    }

    private UserCredentials mockUserCredentials() {
        var userCredentials = new UserCredentials();
        return userCredentials.setId(ID)
                .setUsername(USERNAME)
                .setHashedPassword(PASSWORD);
    }

    private NewTokenResponse mockNewTokenResponse() {
        return new NewTokenResponse()
                .setTokenString(TOKEN);
    }

    private UsernamePasswordAuthenticationToken mockToken() {
        return new UsernamePasswordAuthenticationToken(new DetailModel(), "", List.of());
    }

    private UserCredentialsRequest mockUserCredentialsRequest() {
        return new UserCredentialsRequest()
                .setUsername(USERNAME)
                .setPassword(PASSWORD)
                .setAuthorities(List.of());
    }

    private DetailModel mockDetailModel() {
        var model = new DetailModel();
        model.setUsername(USERNAME);
        model.setPassword(null);
        model.setAuthorities(List.of());

        return model;
    }

    private UpdateUserNameRequest mockUpdateUserNameRequest() {
        return new UpdateUserNameRequest()
                .setCurrentUsername(USERNAME)
                .setNewUserName(NEW_USERNAME);
    }

    private UpdateUserPasswordRequest mockUpdateUserPasswordRequest() {
        return new UpdateUserPasswordRequest()
                .setUsername(USERNAME)
                .setNewPassword(NEW_USERNAME);
    }

    private RemoveUserCredentialsRequest mockRemoveUserCredentialsRequest() {
        return new RemoveUserCredentialsRequest()
                .setUsername(USERNAME);
    }

    private UpdateResult mockUpdateResult(){
        return UpdateResult.acknowledged(1L, 1L, new BsonString("id"));
    }

    private DeleteResult mockDeleteResult() {
        return DeleteResult.acknowledged(1L);
    }

}
