package org.authservice.jwt.service;

import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.SignedJWT;
import org.assertj.core.api.Assertions;
import org.authservice.generate.RetrieveKey;
import org.authservice.generate.keys.HmacKey;
import org.authservice.jwt.TokenService;
import org.authservice.jwt.dto.JWTPayloadData;
import org.authservice.jwt.dto.CreateTokenData;
import org.authservice.login.dto.UserClaim;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.TestPropertySource;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import static org.mockito.Mockito.when;

@TestPropertySource("classpath:/application-test.properties")
class TokenServiceTest {
    private static final String USERNAME = "username";
    private static final String ISSUER = "issuer";
    private static final String AUDIENCE = "audience";
    private static final String SUBJECT = "subject";
    private SecretKey secretKey;
    @Mock
    private RetrieveKey retrieveKey;
    @InjectMocks
    private TokenService tokenService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        this.secretKey = mockSecretKey(HmacKey.HMACSHA256);
    }

    @Test
    void issueToken_shouldIssue_token() {
        var data = mockCreateTokenData(1);
        var token = tokenService.issueToken(data);

        Assertions.assertThat(token)
                .isInstanceOf(SignedJWT.class);
    }

    @Test
    void tokenValid_shouldReturnTrue_ifTokenValid() {
        var data = mockCreateTokenData(1);
        var token = tokenService.issueToken(data);
        when(retrieveKey.getCurrentKey()).thenReturn(this.secretKey);
        when(retrieveKey.getExpiredKey()).thenReturn(this.secretKey);
        var result = tokenService.tokenValid(token.serialize());

        Assertions.assertThat(result)
                .isTrue();
    }

    @Test
    void tokenExpired_shouldReturnTrue_ifTokenExpired() {
        var token = tokenService.issueToken(mockCreateTokenData(-5));
        var expired = tokenService.tokenExpired(token);

        Assertions.assertThat(expired)
                .isTrue();
    }

    @Test
    void extractUsername_shouldReturn_username() {
        var token = tokenService.issueToken(mockCreateTokenData(1));
        var username = tokenService.extractUsername(token.serialize());

        Assertions.assertThat(username)
                .isEqualTo(USERNAME);
    }

    private CreateTokenData mockCreateTokenData(int expiryDays) {
        var tokenData = new CreateTokenData();
        var signer = mockSigner(this.secretKey);
        var algorithm = new HmacKey()
                .getAlgorithm(HmacKey.HMACSHA256);
        var data = mockPayloadData(expiryDays)
                .setExpiry(expiryDays);

        return tokenData.setPayloadData(data)
                .setSigner(signer)
                .setAlgorithm(algorithm);
    }

    private JWTPayloadData mockPayloadData(int expiryDays) {
        var payloadData = new JWTPayloadData();
        var userClaim = new UserClaim();
        userClaim.setUsername(USERNAME);

        payloadData.setSubject(SUBJECT)
                .setIssuer(ISSUER)
                .setAudience(AUDIENCE)
                .setIssued()
                .setExpiry(expiryDays)
                .setClaim(userClaim);

        return payloadData;
    }

    private MACSigner mockSigner(SecretKey secretKey) {
        try {
            return new MACSigner(secretKey);

        } catch (KeyLengthException e) {
            throw new RuntimeException("signer could not be created for CreateTokenData TokenServiceTest", e);

        }
    }

    public SecretKey mockSecretKey(String algorithm) {
        try {
            var keyGen = KeyGenerator.getInstance(algorithm);
            keyGen.init(new SecureRandom());
            return keyGen.generateKey();

        } catch (NoSuchAlgorithmException exception) {
            var message = "Key pair for TokenServiceTest was not successfully generated";
            throw new RuntimeException(message);

        }
    }


}
