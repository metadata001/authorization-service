package org.authservice.common.exceptions;

public class InvalidAlgorithmException extends RuntimeException {

    public InvalidAlgorithmException(String message) {
        super(message, new RuntimeException());
    }

}
