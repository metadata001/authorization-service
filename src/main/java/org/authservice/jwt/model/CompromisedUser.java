package org.authservice.jwt.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import javax.persistence.GeneratedValue;
import java.util.ArrayList;
import java.util.List;

public class CompromisedUser {
    @Id
    @GeneratedValue
    private String id;
    @Field(name = "username")
    private String username;
    @Field(name = "details")
    private List<CompromisedUserDetails> details;

    public CompromisedUser() {}

    public CompromisedUser(String username, List<CompromisedUserDetails> details) {
        this.username = username;
        this.details = details;
    }

    public String getId() {
        return id;
    }

    public CompromisedUser setId(String id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public CompromisedUser setUsername(String username) {
        this.username = username;
        return this;
    }

    public List<CompromisedUserDetails> getDetails() {
        return details;
    }

    public CompromisedUser setDetails(List<CompromisedUserDetails> details) {
        this.details = details;
        return this;
    }

    public CompromisedUser addDetails(CompromisedUserDetails details) {
        if(this.details == null) {
            this.details = new ArrayList<>();
        }
        this.details.add(details);
        return this;
    }

}
