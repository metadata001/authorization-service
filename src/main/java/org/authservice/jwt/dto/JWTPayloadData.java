package org.authservice.jwt.dto;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class JWTPayloadData {
    private static final String ISSUER = "issuer";
    private static final String AUDIENCE = "audience";
    private static final String ISSUED = "issued";
    private static final String EXPIRY = "expiry";
    private static final String CLAIM = "claim";
    private static final String SUBJECT = "subject";
    private HashMap<String, Object> payloadMap;

    public JWTPayloadData() {
        initializeMap();
    }

    public String getIssuer() {
        return (String) this.payloadMap.get(ISSUER);
    }

     public JWTPayloadData setIssuer(String issuer) {
        this.payloadMap.replace(ISSUER, issuer);
        return this;
    }

    public String getAudience() {
        return (String) this.payloadMap.get(AUDIENCE);
    }

    public JWTPayloadData setAudience(String audience) {
        this.payloadMap.replace(AUDIENCE, audience);
        return this;
    }

    public Date getIssued() {
        return (Date) this.payloadMap.get(ISSUED);
    }

    public JWTPayloadData setIssued() {
        this.payloadMap.replace(ISSUED, new Date());
        return this;
    }

    public Date getExpiry() {
        return (Date) this.payloadMap.get(EXPIRY);
    }

    public JWTPayloadData setExpiry(int numberOfDays) {
        var calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, numberOfDays);
        Date date = calendar.getTime();
        this.payloadMap.replace(EXPIRY, date);
        return this;
    }

    public Object getClaim() {
        return this.payloadMap.get(CLAIM);
    }

    public JWTPayloadData setClaim(Object claim) {
        this.payloadMap.replace(CLAIM, claim);
        return this;
    }

    public String getSubject() {
        return (String) this.payloadMap.get(SUBJECT);
    }

    public JWTPayloadData setSubject(String subject) {
        this.payloadMap.replace(SUBJECT, subject);
        return this;
    }

    public HashMap<String,Object> getPayload() {
        return payloadMap;
    }

    private void initializeMap() {
        this.payloadMap = new HashMap<>();
        this.payloadMap.put(ISSUER, null);
        this.payloadMap.put(AUDIENCE, null);
        this.payloadMap.put(ISSUED, null);
        this.payloadMap.put(EXPIRY, null);
        this.payloadMap.put(CLAIM, null);
        this.payloadMap.put(SUBJECT, null);
    }

}
