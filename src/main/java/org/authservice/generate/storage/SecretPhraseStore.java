package org.authservice.generate.storage;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SecretPhraseStore implements Store {
    private static final String CURRENT = "current";
    private static final String EXPIRED = "expired";
    private static final String CURRENT_FILEPATH = ".\\src\\main\\resources\\current_keystore.txt";
    private static final String EXPIRED_FILEPATH = ".\\src\\main\\resources\\expired_keystore.txt";
    @Value("${keystore_pwd}")
    private String password;

    @Override
    public String getExpiredFilepath() {
        return EXPIRED_FILEPATH;
    }

    @Override
    public String getCurrentFilepath() {
        return CURRENT_FILEPATH;
    }

    @Override
    public String getCurrentAlias() {
        return CURRENT;
    }

    @Override
    public String getExpiredAlias() {
        return EXPIRED;
    }

    @Override
    public String getPassword() {
        return this.password;
    }
}
