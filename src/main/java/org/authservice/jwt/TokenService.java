package org.authservice.jwt;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.shaded.gson.internal.LinkedTreeMap;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.authservice.generate.RetrieveKey;
import org.authservice.jwt.dto.CreateTokenData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.crypto.SecretKey;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

@Component
public class TokenService {
    private static final String CLAIM = "claim";
    private static final String TEXT_PLAIN = "text/plain";
    private final Logger logger = LoggerFactory.getLogger(TokenService.class);
    private final RetrieveKey retrieveKey;

    @Autowired
    public TokenService(RetrieveKey retrieveKey) {
        this.retrieveKey = retrieveKey;
    }

    public SignedJWT issueToken(CreateTokenData data) {
        var header = constructJwsHeader(data);
        var claimsSet = constructClaimsSet(data);
        var token = constructUnsignedToken(header, claimsSet);

        return signToken(token, data.getSigner());
    }

    private JWSHeader constructJwsHeader(CreateTokenData data) {
        return new JWSHeader.Builder(data.getAlgorithm())
                .contentType(TEXT_PLAIN)
                .type(JOSEObjectType.JWT)
                .build();
    }

    private JWTClaimsSet constructClaimsSet(CreateTokenData data) {
        return new JWTClaimsSet.Builder()
                .issuer(data.getPayloadData().getIssuer())
                .audience(data.getPayloadData().getAudience())
                .issueTime(data.getPayloadData().getIssued())
                .expirationTime(data.getPayloadData().getExpiry())
                .claim(CLAIM, data.getPayloadData().getClaim())
                .subject(data.getPayloadData().getSubject())
                .build();
    }

    private SignedJWT constructUnsignedToken(JWSHeader header, JWTClaimsSet claimsSet) {
        return new SignedJWT(header, claimsSet);
    }

    private SignedJWT signToken(SignedJWT token, JWSSigner signer) {
        try {
            token.sign(signer);
            return token;

        } catch (JOSEException e) {
            var message = "token could not be signed";
            logger.warn(message);
            throw new RuntimeException(message, e);

        }
    }

    public boolean tokenValid(String tokenString) {
        var currentKeyVerified = verify(retrieveKey.getCurrentKey(), tokenString);
        var expiredKeyVerified = verify(retrieveKey.getExpiredKey(), tokenString);

        return currentKeyVerified || expiredKeyVerified;
    }

    public boolean tokenExpired(SignedJWT token) {
        try {
            var calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            var currentDate = calendar.getTime();
            var claims = token.getJWTClaimsSet();
            return currentDate.after(claims.getExpirationTime());

        } catch (ParseException e) {
            var message = "claims set was not successfully extracted from singed jwt";
            logger.warn(message);
            throw new RuntimeException(message, e);

        }
    }

    public SignedJWT parseToSignedToken(String tokenString) {
        try {
            return SignedJWT.parse(tokenString);

        } catch (ParseException e) {
            throw new RuntimeException("tokenString could not be converted to signed jwt", e);

        }
    }

    public String extractUsername(String tokenString) {
        try {
            var token = SignedJWT.parse(tokenString);
            var userClaim = (LinkedTreeMap) token.getJWTClaimsSet().getClaim(CLAIM);
            return (String) userClaim.values().toArray()[0];

        } catch (ParseException e) {
            var message = "token could not be parsed";
            logger.warn(message);
            throw new RuntimeException(message, e);

        }
    }

    private boolean verify(SecretKey key, String tokenString) {
        try {
            var jwsObject = JWSObject.parse(tokenString);
            var verifier = new MACVerifier(key);
            return jwsObject.verify(verifier);

        } catch (ParseException | JOSEException e) {
            var message = "could not verify token due to error";
            logger.warn(message);
            throw new RuntimeException(e);

        }
    }

}
