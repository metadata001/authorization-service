package org.authservice.login.dto;

import java.io.Serializable;

public class UserClaim implements Serializable {
    private String username;

    public UserClaim() {

    }

    public UserClaim(String username) {
        this.username = username;
    }

    public UserClaim setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getUsername() {
        return username;
    }
}
