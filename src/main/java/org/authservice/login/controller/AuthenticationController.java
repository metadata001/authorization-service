package org.authservice.login.controller;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.authservice.login.dto.RemoveUserCredentialsRequest;
import org.authservice.login.dto.UpdateUserNameRequest;
import org.authservice.login.dto.UpdateUserPasswordRequest;
import org.authservice.login.dto.UserCredentialsRequest;
import org.authservice.login.model.DetailModel;
import org.authservice.login.model.UserCredentials;
import org.authservice.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {
    private static final String TOKEN_HEADER = "JWT_TOKEN";
    private final LoginService loginService;

    @Autowired
    public AuthenticationController(LoginService loginService) {
        this.loginService = loginService;
    }

    @PostMapping("/signup")
    public ResponseEntity<UserCredentials> signup(@RequestBody @Valid UserCredentialsRequest request) {
        var result = loginService.signup(request);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/login")
    public ResponseEntity<DetailModel> login(@RequestBody @Valid UserCredentialsRequest request) {
        var result = loginService.login(request);
        var responseHeaders = new HttpHeaders();
        responseHeaders.set(TOKEN_HEADER, result.getToken());
        result.setToken(null);
        return ResponseEntity.ok()
                .headers(responseHeaders)
                .body(result);
    }

    @PutMapping("/update-username")
    public ResponseEntity<DetailModel> updateUsername(@RequestBody @Valid UpdateUserNameRequest request, HttpServletResponse response) {
        var result = loginService.updateUsername(request);
        response.setHeader(TOKEN_HEADER, result.getToken());
        return ResponseEntity.ok(result);
    }

    @PutMapping("/update-password")
    public ResponseEntity<UpdateResult> updatePassword(@RequestBody @Valid UpdateUserPasswordRequest request) {
        var result = loginService.updatePassword(request);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/remove-credentials")
    public ResponseEntity<DeleteResult> removeCredentials(@RequestBody @Valid RemoveUserCredentialsRequest request, HttpServletResponse response) {
        var result = loginService.removeCredentials(request);
        return ResponseEntity.ok(result);
    }

}
