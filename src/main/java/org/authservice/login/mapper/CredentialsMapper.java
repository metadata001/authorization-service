package org.authservice.login.mapper;

import org.authservice.login.dto.UserCredentialsRequest;
import org.authservice.login.model.UserCredentials;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CredentialsMapper {

    @Mapping(target = "hashedPassword", source = "password")
    @Mapping(target = "authorities", source = "authorities")
    @Mapping(target = "id", ignore = true)
    UserCredentials map(UserCredentialsRequest request);

}
