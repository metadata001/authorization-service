package org.authservice.jwt.controller;

import jakarta.validation.Valid;
import org.authservice.jwt.model.*;
import org.authservice.jwt.TokenValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/jwt")
public class JsonWebTokenController {
    private final TokenValidationService tokenValidationService;

    @Autowired
    public JsonWebTokenController(TokenValidationService tokenValidationService) {
        this.tokenValidationService = tokenValidationService;
    }

    @PostMapping("/validate-token")
    public ResponseEntity<ValidatedTokenResponse> validateToken(@RequestBody @Valid ValidateTokenRequest request) {
        var result = tokenValidationService.validateToken(request.getTokenString());
        return ResponseEntity.ok(result);
    }

    @PostMapping("/issue-token")
    public ResponseEntity<NewTokenResponse> issueToken(@RequestBody @Valid IssueTokenRequest request) {
        var result = tokenValidationService.issueToken(request.getUsername());
        return ResponseEntity.ok(result);
    }

    @GetMapping("/retrieve-compromised-token")
    public ResponseEntity<CompromisedTokensResponse> removeCredentials(@PathVariable("user") String username) {
        var result = tokenValidationService.retrieveCompromisedUser(username);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/retrieve-compromised-tokens")
    public ResponseEntity<CompromisedTokensResponse> retrieveCompromisedTokens() {
        var result = tokenValidationService.retrieveAllCompromisedUsers();
        return ResponseEntity.ok(result);
    }

}
