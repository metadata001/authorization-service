package org.authservice.config;

import org.authservice.jwt.TokenValidationService;
import org.authservice.login.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import java.util.List;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {
    private static final String LOGIN_PATH = "/auth/login";
    private static final String REGISTERED_PATH = "/auth/create-registered";
    private static final String ADMIN_PATH = "/auth/create-admin";
    private static final String CORS_PATH = "/**";
    private static final String CORS = "*";
    private final AuthenticationConfiguration authenticationConfiguration;
    private final UserDetailService userDetailService;
    private final TokenValidationService tokenValidationService;
    private final Logger logger = LoggerFactory.getLogger(SecurityConfiguration.class);

    @Autowired
    public SecurityConfiguration(AuthenticationConfiguration authenticationConfiguration,
                                 TokenValidationService tokenValidationService,
                                 UserDetailService userDetailService) {
        this.authenticationConfiguration = authenticationConfiguration;
        this.tokenValidationService = tokenValidationService;
        this.userDetailService = userDetailService;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2B, 10);
    }

    public AuthenticationProvider daoAuthProvider() {
        var provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(this.userDetailService);
        provider.setPasswordEncoder(this.passwordEncoder());
        return provider;
    }

    public AuthenticationProvider jwtAuthProvider() {
        var provider = new JwtAuthenticationProvider(tokenValidationService);
        provider.setUserDetailService(this.userDetailService);
        return provider;
    }

    @Bean
    public ProviderManager providerManager(ProviderManager providerManager) {
        providerManager.getProviders().add(this.jwtAuthProvider());
        providerManager.getProviders().add(this.daoAuthProvider());
        return providerManager;
    }

    @Bean
    public AuthenticationManager authenticationManager() throws Exception {
        return this.authenticationConfiguration.getAuthenticationManager();
    }

    @Bean

    public JwtAuthenticationFilter jwtAuthenticationFilter() throws Exception {
        try {
            return new JwtAuthenticationFilter(this.tokenValidationService, this.authenticationManager());

        } catch (Exception e) {
            throw new Exception("authentication manager could not be instantiated", e);

        }
    }


    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) {
        try {
            http.csrf(AbstractHttpConfigurer::disable)
                    .cors(AbstractHttpConfigurer::disable);

            http.authorizeHttpRequests(requests -> requests
                    .requestMatchers(LOGIN_PATH, REGISTERED_PATH, ADMIN_PATH)
                    .permitAll()
            )
                    .authenticationProvider(this.daoAuthProvider());

            http.authorizeHttpRequests(requests -> requests
                    .anyRequest()
                    .authenticated()
            )
                    .authenticationProvider(this.jwtAuthProvider());

            http.sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                    .addFilterBefore(this.jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

            return http.build();

        } catch (Exception e) {
            var message = "http configuration was not successful";
            logger.warn(message);
            throw new RuntimeException(e);

        }
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        var corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowedOrigins(List.of(CORS));
        corsConfiguration.setAllowedMethods(List.of(CORS));
        corsConfiguration.setAllowedHeaders(List.of(CORS));
        corsConfiguration.setAllowCredentials(true);
        var source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration(CORS_PATH, corsConfiguration);

        return source;
    }

}
