package org.authservice.jwt.model;

public class NewTokenResponse {
    private String tokenString;

    public String getTokenString() {
        return tokenString;
    }

    public NewTokenResponse setTokenString(String tokenString) {
        this.tokenString = tokenString;
        return this;
    }

}
