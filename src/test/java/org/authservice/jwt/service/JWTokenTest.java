package org.authservice.jwt.service;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import org.assertj.core.api.Assertions;
import org.authservice.common.exceptions.PayloadDataException;
import org.authservice.generate.keys.HmacKey;
import org.authservice.jwt.dto.JWTPayloadData;
import org.authservice.jwt.dto.JWToken;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.HashMap;

class JWTokenTest {
    private static final String ISSUER = "issuer";
    private static final String AUDIENCE = "audience";
    private static final String USERNAME = "username";
    private SecretKey secretKey;
    private JWToken token;

    @BeforeEach
    void setup() {
        token = new JWToken();
        this.secretKey = mockSecretKey(HmacKey.HMACSHA256);
    }

    @Test
    void JWTokenSetHeader_shouldSet_header() {
        token.setHeader(JWSAlgorithm.HS256);

        Assertions.assertThat(mockHeader().getAlgorithm())
                .isEqualTo(token.getHeader().getAlgorithm());
    }

    @Test
    void JWTokenSetPayloadData_shouldSet_payloadData() {
        token.setPayloadData(mockPayloadData());

        Assertions.assertThat(token.getPayloadData().getPayload())
                .containsValues(ISSUER, AUDIENCE, mockPayloadData().getClaim());
    }

    @Test
    void JWTokenSetPayload_shouldSet_payload() {
        token.setPayloadData(mockPayloadData())
                        .setPayload();

        Assertions.assertThat(token.getPayload())
                .isNotNull();
    }

    @Test
    void JWTokenSetPayload_shouldThrowException_whenPayloadDataIsNotSet() {
        Assertions.assertThatThrownBy(() ->
                token.setPayload()
        ).isInstanceOf(PayloadDataException.class);
    }

    @Test
    void JWTokenSetMacSigner_shouldSet_macSigner() {
        token.setMacSigner(this.secretKey);

        Assertions.assertThat(token.getJwsSigner())
                .isNotNull();
    }

    @Test
    void JWTokenCreate_shouldCreate_jwsObject() {
        token.setHeader(JWSAlgorithm.HS256)
                .setPayloadData(mockPayloadData())
                .setPayload()
                .setMacSigner(this.secretKey)
                .createToken();

        Assertions.assertThat(token.getToken())
                .isNotNull();
    }

    @Test
    void signToken_shouldSign_token() {
        token.setHeader(JWSAlgorithm.HS256)
                .setPayloadData(mockPayloadData())
                .setPayload()
                .setMacSigner(this.secretKey)
                .createToken()
                .signToken();

        Assertions.assertThat(token.getToken().getSignature())
                .isNotNull();
    }

    @Test
    void getTokenString_shouldReturn_validTokenString() {
        var tokenString = token.setHeader(JWSAlgorithm.HS256)
                .setPayloadData(mockPayloadData())
                .setPayload()
                .setMacSigner(this.secretKey)
                .createToken()
                .signToken()
                .getTokenString();

        Assertions.assertThat(tokenString)
                .isNotEmpty()
                .isNotNull();
    }

    private JWSHeader mockHeader() {
        return new JWSHeader(JWSAlgorithm.HS256);
    }

    private JWTPayloadData mockPayloadData() {
        var payloadData = new JWTPayloadData();
        var info = new HashMap<String,Object>();
        info.put(USERNAME, USERNAME);

        payloadData.setIssuer(ISSUER)
                .setAudience(AUDIENCE)
                .setIssued()
                .setExpiry(7)
                .setClaim(info);

        return payloadData;
    }

    public SecretKey mockSecretKey(String algorithm) {
        try {
            var keyGen = KeyGenerator.getInstance(algorithm);
            keyGen.init(new SecureRandom());
            return keyGen.generateKey();

        } catch (NoSuchAlgorithmException exception) {
            var message = "Key pair for TokenServiceTest was not successfully generated";
            throw new RuntimeException(message);

        }
    }

}
