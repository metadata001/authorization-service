package org.authservice.login.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import static java.util.stream.Collectors.toCollection;

public class DetailModel implements UserDetails {
    private String username;
    private String password;
    private List<SimpleGrantedAuthority> authorities;
    private String token;

    public DetailModel() {
        this.username = null;
        this.authorities = List.of();
    }

    public DetailModel(UserCredentials credentials) {
        this.username = credentials.getUsername();
        this.password = credentials.getHashedPassword();
        this.authorities = checkAuthorities(credentials);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Objects.requireNonNullElse(this.authorities, List.of());
    }

    public void setGrantedAuthorities(List<String> authorities ) {
        this.authorities = authorities.stream()
                .map(SimpleGrantedAuthority::new)
                .toList();
    }

    public void setAuthorities(List<SimpleGrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public void setPassword(String hashedPassword) {
        this.password = hashedPassword;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return UserDetails.super.isAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return UserDetails.super.isAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return UserDetails.super.isCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return UserDetails.super.isEnabled();
    }

    public void setToken(String tokenString) {
        this.token = tokenString;
    }

    public String getToken() {
        return token;
    }

    private List<SimpleGrantedAuthority> checkAuthorities(UserCredentials credentials) {

        if(credentials.getAuthorities() != null && !credentials.getAuthorities().isEmpty()) {
            return credentials.getAuthorities().stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(toCollection(ArrayList<SimpleGrantedAuthority>::new));
        }
        else {
            return new ArrayList<>();
        }

    }

}
