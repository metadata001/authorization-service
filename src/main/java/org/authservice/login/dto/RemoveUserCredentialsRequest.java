package org.authservice.login.dto;

public class RemoveUserCredentialsRequest {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public RemoveUserCredentialsRequest setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public RemoveUserCredentialsRequest setPassword(String password) {
        this.password = password;
        return this;
    }

}
