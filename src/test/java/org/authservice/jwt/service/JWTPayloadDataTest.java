package org.authservice.jwt.service;

import org.assertj.core.api.Assertions;
import org.authservice.jwt.dto.JWTPayloadData;
import org.junit.jupiter.api.Test;
import java.util.HashSet;
import java.util.Set;

class JWTPayloadDataTest {
    private static final String ISSUER = "issuer";
    private static final String AUDIENCE = "audience";
    private static final String ISSUED = "issued";
    private static final String EXPIRY = "expiry";
    private static final String CLAIM = "claim";
    private static final String SUBJECT = "subject";

    @Test
    void payloadData_shouldInitialize_payloadMapWithKeys() {
        var data = new JWTPayloadData();
        var keys = data.getPayload().keySet();
        Assertions.assertThat(keys)
                .usingRecursiveComparison()
                .isEqualTo(payloadKeys());
    }

    private Set<String> payloadKeys() {
        var set = new HashSet<String>();
        set.add(ISSUER);
        set.add(AUDIENCE);
        set.add(ISSUED);
        set.add(EXPIRY);
        set.add(CLAIM);
        set.add(SUBJECT);
        return set;
    }

}
