package org.authservice.common.helper;

import io.micrometer.common.util.StringUtils;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.lang.Nullable;

public class NonNullUpdate extends Update {
    @Override
    public Update set(String key, @Nullable Object value) {
        if(value instanceof String && StringUtils.isNotBlank(value.toString())) {
            addMultiFieldOperation("$set", key, value);
            return this;
        }
        return null;
    }
}
