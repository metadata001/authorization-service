package org.authservice.generate;

import org.authservice.generate.storage.SecretPhraseStore;
import org.authservice.generate.storage.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

@Component
public class StoreKeys {
    private final Store storage;
    private static final Logger logger = LoggerFactory.getLogger(StoreKeys.class);

    public StoreKeys(SecretPhraseStore storage) {
        this.storage = storage;
    }

    protected void saveSecretKey(SecretKey newSecretKey) {
        var file = new File(storage.getCurrentFilepath());

        try(var input = new FileInputStream(file)) {
            //gets the instance of the keystore
            var keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

            if (file.length() > 0) {
                //loads current keystore, extracts key and saves it to expired keystore
                keyStore.load(input, storage.getPassword().toCharArray());
                var currentSecretKey = getSecretKey(keyStore);
                setExpiredKey(currentSecretKey);

            }
            setCurrentKey(newSecretKey);
            logger.warn("all keys were successfully saved to keystore");

        } catch (KeyStoreException | IOException | NoSuchAlgorithmException | CertificateException e) {
            logger.warn("encountered an error while trying to save keys to keystores");
            logger.warn(e.toString());

        }

    }

    private void setExpiredKey(SecretKey currentSecretKey) {
        var file = new File(storage.getExpiredFilepath());

        try(var output = new FileOutputStream(file, false)) {
            //gets the instance of keystore and sets password param
            var keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            KeyStore.ProtectionParameter param = new KeyStore.PasswordProtection(storage.getPassword().toCharArray());

            //sets the current key to the expired key
            keyStore.load(null, storage.getPassword().toCharArray());
            var expiredSkEntry = new KeyStore.SecretKeyEntry(currentSecretKey);
            keyStore.setEntry(storage.getExpiredAlias(), expiredSkEntry, param);
            keyStore.store(output, storage.getPassword().toCharArray());
            logger.warn("current key was successfully set to expired key");

        } catch (CertificateException | IOException | NoSuchAlgorithmException | KeyStoreException e) {
            logger.warn("could not set current key to expired key");
            logger.warn(e.toString());

        }

    }

    private void setCurrentKey(SecretKey newSecretKey) {
        var file = new File(storage.getCurrentFilepath());

        try(var output = new FileOutputStream(file)) {
            //gets the instance of keystore and sets password param
            var keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            KeyStore.ProtectionParameter param = new KeyStore.PasswordProtection(storage.getPassword().toCharArray());

            //sets the new secret key to current
            var skEntry = new KeyStore.SecretKeyEntry(newSecretKey);
            keyStore.load(null, storage.getPassword().toCharArray());
            keyStore.setEntry(storage.getCurrentAlias(), skEntry, param);
            keyStore.store(output, storage.getPassword().toCharArray());
            logger.warn("new key successfully set to current key");

        } catch (CertificateException | KeyStoreException | IOException | NoSuchAlgorithmException e) {
            logger.warn("could not set new key to current key");
            logger.warn(e.toString());

        }

    }

    protected SecretKey getSecretKey(KeyStore keyStore) {
        try {
            KeyStore.ProtectionParameter param = new KeyStore.PasswordProtection(storage.getPassword().toCharArray());
            var entry = (KeyStore.SecretKeyEntry) keyStore.getEntry(storage.getCurrentAlias(), param);
            return entry.getSecretKey();

        } catch (NoSuchAlgorithmException | UnrecoverableEntryException | KeyStoreException e) {
            logger.warn("failed to recover key from keystore");
            logger.warn(e.toString());

        }

        return null;
    }

}
