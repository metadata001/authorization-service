package org.authservice.jwt.model;

public class ValidateTokenRequest {
    private String tokenString;

    public String getTokenString() {
        return tokenString;
    }

    public ValidateTokenRequest setTokenString(String tokenString) {
        this.tokenString = tokenString;
        return this;
    }

}
