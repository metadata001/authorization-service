package org.authservice.common.helper;

import org.springframework.stereotype.Component;
import java.lang.reflect.Field;
import java.util.*;

@Component
public class ErrorValidationKeys {
    public static final String PASSWORD = "password";
    public static final String USERNAME = "username";

    /**
     * @return Map - maps validations seen above as keys to map with no values.
     * <br><br>
     * This class maps targets which are the field values of this class, to spring
     * annotation validation messages. Each field is evaluated in the get-targets
     * method below, set to lower-case, then mapped to a <b>{@code HashMap<String,<Set<String>>}</b>
     * ensuring that the values are unique for each key.
     * <br><br>
     * This map is then consumed by the ControllerExceptionHandler class, which maps
     * the messages to the target keys. This provides a framework for handling and passing
     * validation errors to the UI, through the application's endpoints.
     * <br><br>
     * This class works by making a spring validation annotation with a message for a string
     * field in a class, e.g. @NotNull(message =  name cannot be empty);
     * After creating the annotation, a public static final String <b>NAME</b> variable
     * should be made in this class.
     */
    public static Map<String, Set<String>> getTargets() {
        Field[] fields = ErrorValidationKeys.class.getDeclaredFields();
        var map = new HashMap<String,Set<String>>();

        for(Field field : fields) {
            field.setAccessible(true);
            map.put(field.getName().toLowerCase(), new HashSet<>());
        }
        return map;
    }

}
