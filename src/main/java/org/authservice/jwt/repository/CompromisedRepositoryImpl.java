package org.authservice.jwt.repository;

import org.authservice.jwt.model.CompromisedUser;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
@Qualifier("compromised")
public class CompromisedRepositoryImpl {
    private final MongoTemplate template;

    public CompromisedRepositoryImpl(MongoTemplate template) {
        this.template = template;
    }

    public Optional<CompromisedUser> findOne(Query query) {
        var result = template.findOne(query, CompromisedUser.class);
        return Optional.ofNullable(result);
    }

    public Optional<List<CompromisedUser>> findAll() {
        var result = template.findAll(CompromisedUser.class);
        return Optional.of(result);
    }

    public Optional<CompromisedUser> save(CompromisedUser compromisedUser) {
        var result = template.save(compromisedUser);
        return Optional.of(result);
    }

}
