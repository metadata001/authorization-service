package org.authservice.login.service.query;

import org.authservice.common.helper.NonNullUpdate;
import org.authservice.login.dto.UpdateUserNameRequest;
import org.authservice.login.dto.UpdateUserPasswordRequest;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import static org.authservice.common.helper.ErrorValidationKeys.PASSWORD;
import static org.authservice.common.helper.ErrorValidationKeys.USERNAME;

@Component
public class CredentialsUpdate {

    public static Update getUserCredentialsUpdate(UpdateUserNameRequest request) {
        var update = new NonNullUpdate();
        update.set(USERNAME, request.getNewUserName());
        return update;
    }

    public static Update getUserCredentialsUpdate(UpdateUserPasswordRequest request) {
        var update = new NonNullUpdate();
        update.set(PASSWORD, request.getNewPassword());
        return update;
    }

}