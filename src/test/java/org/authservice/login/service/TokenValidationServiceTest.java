package org.authservice.login.service;

import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.SignedJWT;
import org.assertj.core.api.Assertions;
import org.authservice.generate.RetrieveKey;
import org.authservice.generate.keys.HmacKey;
import org.authservice.jwt.model.CompromisedUser;
import org.authservice.jwt.CompromisedTokenService;
import org.authservice.jwt.TokenService;
import org.authservice.jwt.TokenValidationService;
import org.authservice.jwt.dto.CreateTokenData;
import org.authservice.jwt.dto.JWTPayloadData;
import org.authservice.login.dto.UserClaim;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class TokenValidationServiceTest {
    private static final String USERNAME = "username";
    private static final String TOKEN = "token";
    private static final String ISSUER = "issuer";
    private static final String AUDIENCE = "audience";
    private static final String SUBJECT = "subject";
    private SecretKey secretKey;
    @Captor
    ArgumentCaptor<String> usernameCaptor;
    @Captor
    ArgumentCaptor<String> tokenStringCaptor;
    @Mock
    private TokenService tokenService;
    @Mock
    private RetrieveKey retrieveKey;
    @Mock
    private CompromisedTokenService compromisedTokenService;
    @InjectMocks
    private TokenValidationService tokenValidationService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);
        this.secretKey = mockSecretKey(HmacKey.HMACSHA256);
    }

    @Test
    void validateToken_shouldReturnTrue_whenTokenExpired() {
        when(tokenService.tokenExpired(any())).thenReturn(true);
        var result = tokenValidationService.validateToken(anyString());

        Assertions.assertThat(result.isExpired())
                .isTrue();
    }

    @Test
    void validateToken_shouldStoreCompromisedToken_whenTokenInvalid() {
        when(tokenService.parseToSignedToken(anyString())).thenReturn(mock(SignedJWT.class));
        when(tokenService.tokenExpired(any())).thenReturn(false);
        when(tokenService.extractUsername(anyString())).thenReturn(USERNAME);
        when(tokenService.tokenValid(any())).thenReturn(false);
        tokenValidationService.validateToken(TOKEN);

        verify(compromisedTokenService).storeCompromisedTokenDetails(usernameCaptor.capture(), tokenStringCaptor.capture());

        var username = usernameCaptor.getValue();
        Assertions.assertThat(username)
                .isEqualTo(USERNAME);

        var tokenString = tokenStringCaptor.getValue();
        Assertions.assertThat(tokenString)
                .isEqualTo(TOKEN);
    }

    @Test
    void validateToken_shouldReturnTrue_whenTokenValid() {
        when(tokenService.tokenValid(any())).thenReturn(true);
        when(tokenService.tokenValid(any())).thenReturn(true);
        var result = tokenValidationService.validateToken(anyString());

        Assertions.assertThat(result.isValid())
                .isTrue();
    }
    
    @Test
    void issueToken_shouldReturnToken_inNewTokenResponse() {
        when(tokenService.issueToken(any())).thenReturn(mock(SignedJWT.class));
        when(retrieveKey.getCurrentKey()).thenReturn(mock(SecretKey.class));
        var result = tokenValidationService.issueToken(USERNAME);
        
        Assertions.assertThat(result)
                .isNotNull();
    }

    @Test
    void retrieveCompromisedUser_shouldReturn_CompromisedUser() {
        when(compromisedTokenService.retrieveCompromisedUser(anyString()))
                .thenReturn(mock(CompromisedUser.class));

        var result = tokenValidationService
                .retrieveCompromisedUser(USERNAME);

        Assertions.assertThat(result.getCompromisedToken())
                .isNotNull();
    }

    @Test
    void retrieveAllCompromisedUsers_shouldReturn_CompromisedUsersList() {
        when(compromisedTokenService.retrieveAllCompromisedUsers())
                .thenReturn(List.of(mock(CompromisedUser.class)));

        var result = tokenValidationService
                .retrieveAllCompromisedUsers();

        Assertions.assertThat(result.getCompromisedTokens().get(0))
                .isNotNull();
    }

    @Test
    void storeCompromisedUser_shouldReturn_storedCompromisedUser() {
        when(compromisedTokenService.storeCompromisedTokenDetails(anyString(), anyString()))
                .thenReturn(mock(CompromisedUser.class));

        var result = tokenValidationService.storeCompromisedToken(USERNAME, TOKEN);

        Assertions.assertThat(result)
                .isNotNull();
    }

    @Test
    void extractUsername_shouldReturn_username() {
        when(tokenService.extractUsername(anyString())).thenReturn(USERNAME);

        var result = tokenValidationService.extractUsername(USERNAME);

        Assertions.assertThat(result)
                .isEqualTo(USERNAME);
    }

    private CreateTokenData mockCreateTokenData(int expiryDays) {
        var tokenData = new CreateTokenData();
        var signer = mockSigner(this.secretKey);
        var algorithm = new HmacKey()
                .getAlgorithm(HmacKey.HMACSHA256);
        var data = mockPayloadData(expiryDays)
                .setExpiry(expiryDays);

        return tokenData.setPayloadData(data)
                .setSigner(signer)
                .setAlgorithm(algorithm);
    }

    private JWTPayloadData mockPayloadData(int expiryDays) {
        var payloadData = new JWTPayloadData();
        var userClaim = new UserClaim();
        userClaim.setUsername(USERNAME);

        payloadData.setSubject(SUBJECT)
                .setIssuer(ISSUER)
                .setAudience(AUDIENCE)
                .setIssued()
                .setExpiry(expiryDays)
                .setClaim(userClaim);

        return payloadData;
    }

    private MACSigner mockSigner(SecretKey secretKey) {
        try {
            return new MACSigner(secretKey);

        } catch (KeyLengthException e) {
            throw new RuntimeException("signer could not be created for CreateTokenData TokenServiceTest", e);

        }
    }

    public SecretKey mockSecretKey(String algorithm) {
        try {
            var keyGen = KeyGenerator.getInstance(algorithm);
            keyGen.init(new SecureRandom());
            return keyGen.generateKey();

        } catch (NoSuchAlgorithmException exception) {
            var message = "Key pair for TokenServiceTest was not successfully generated";
            throw new RuntimeException(message);

        }
    }

}
