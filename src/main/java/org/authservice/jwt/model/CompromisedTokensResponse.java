package org.authservice.jwt.model;

import java.util.List;

public class CompromisedTokensResponse {
    private List<CompromisedUser> compromisedTokens;
    private CompromisedUser compromisedToken;

    public List<CompromisedUser> getCompromisedTokens() {
        return compromisedTokens;
    }

    public CompromisedTokensResponse setCompromisedTokens(List<CompromisedUser> compromisedTokens) {
        this.compromisedTokens = compromisedTokens;
        return this;
    }

    public CompromisedUser getCompromisedToken() {
        return compromisedToken;
    }

    public CompromisedTokensResponse setCompromisedToken(CompromisedUser compromisedToken) {
        this.compromisedToken = compromisedToken;
        return this;
    }

}
