package org.authservice.common.exceptions;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class ErrorResponse {
    private HttpStatus status;
    @JsonProperty("time stamp")
    private String timeStamp;
    private String message;
    private Logger LOGGER = LoggerFactory.getLogger(ErrorResponse.class);

    public ErrorResponse() {
        this.timeStamp = this.formattedDateTime();
    }

    public ErrorResponse(HttpStatus status, String message) {
        this();
        this.status = status;
        this.message = message;
    }

    public ErrorResponse(HttpStatus status, String timeStamp, String message) {
        this.timeStamp = timeStamp;
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getMessage() {
        return message;
    }

    public ErrorResponse setStatus(HttpStatus status) {
        this.status = status;
        return this;
    }

    public ErrorResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    private String formattedDateTime() {
        SimpleDateFormat formatTime = new SimpleDateFormat("hh:mm aa");
        String time = formatTime.format(new Date());
        var zoneId = ZoneId.systemDefault().toString();
        var timeZone = String.valueOf(zoneId);
        var d = LocalDateTime.now();
        var date = d.getDayOfMonth() + "-" + d.getMonth() + "-" +
                   d.getYear() + " " + time + " " + timeZone;
        LOGGER.warn(date);
        return date;
    }


}
