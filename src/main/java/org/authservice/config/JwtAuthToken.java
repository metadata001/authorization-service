package org.authservice.config;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import java.util.List;

public class JwtAuthToken extends AbstractAuthenticationToken {
    private final String principal;
    private final String credentials;

    public JwtAuthToken(String username, String tokenString,
                        List<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = username;
        this.credentials = tokenString;
    }

    public JwtAuthToken(String username, String tokenString) {
        super(List.of());
        this.principal = username;
        this.credentials = tokenString;
    }

    @Override
    public String getPrincipal() {
        return this.principal;
    }

    @Override
    public String getCredentials() {
        return this.credentials;
    }

}
