package org.authservice.jwt;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.crypto.MACSigner;
import org.authservice.generate.RetrieveKey;
import org.authservice.jwt.model.CompromisedTokensResponse;
import org.authservice.jwt.model.CompromisedUser;
import org.authservice.jwt.model.NewTokenResponse;
import org.authservice.jwt.model.ValidatedTokenResponse;
import org.authservice.jwt.dto.CreateTokenData;
import org.authservice.login.dto.UserClaim;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TokenValidationService {
    Logger logger = LoggerFactory.getLogger(TokenValidationService.class);
    private final TokenService tokenService;
    private final RetrieveKey retrieveKey;
    private final CompromisedTokenService compromisedTokenService;

    @Autowired
    public TokenValidationService( CompromisedTokenService compromisedTokenService,
                                   TokenService tokenService, RetrieveKey retrieveKey) {
        this.compromisedTokenService = compromisedTokenService;
        this.tokenService = tokenService;
        this.retrieveKey = retrieveKey;
    }

    public ValidatedTokenResponse validateToken(String tokenString) {
       var username = tokenService.extractUsername(tokenString);
       var signedJwt = tokenService.parseToSignedToken(tokenString);
       var response = new ValidatedTokenResponse();
       response.setTokenString(tokenString)
               .setUsername(username)
               .setValid(false)
               .setExpired(false);

        //checks if token expired
        if (tokenService.tokenExpired(signedJwt)) {
            response.setExpired(true);

        }
        //catches and stores invalid token data
        else if(!tokenService.tokenValid(tokenString)) {
            compromisedTokenService.storeCompromisedTokenDetails(username, tokenString);
            var message = String.format("token for user: %s is invalid, details for user has been stored", username);
            logger.warn(message);

        }
        //validates token
        else if (tokenService.tokenValid(tokenString)) {
            response.setValid(true);

        }
        //throws runtime exception with message if the above conditions do not apply
        else {
            throw new RuntimeException("response could not process because token could not be processed");

        }

        return response;
    }

    public NewTokenResponse issueToken(String username) {
        var tokenData = tokenData(username);
        var token = tokenService.issueToken(tokenData);
        var response = new NewTokenResponse();
        response.setTokenString(token.serialize());

        return response;
    }

    public CompromisedTokensResponse retrieveCompromisedUser(String username) {
        var response = new CompromisedTokensResponse();
        var compromised = this.compromisedTokenService
                .retrieveCompromisedUser(username);
        response.setCompromisedToken(compromised)
                .setCompromisedTokens(null);

        return response;
    }

    public CompromisedTokensResponse retrieveAllCompromisedUsers() {
        var response = new CompromisedTokensResponse();
        var compromisedList = this.compromisedTokenService
                .retrieveAllCompromisedUsers();
        response.setCompromisedTokens(compromisedList)
                .setCompromisedToken(null);

        return response;
    }

    public CompromisedUser storeCompromisedToken(String username, String tokenString) {
        return this.compromisedTokenService
                .storeCompromisedTokenDetails(username, tokenString);
    }

    public String extractUsername(String tokenString) {
        return this.tokenService.extractUsername(tokenString);
    }

    private CreateTokenData tokenData(String username) {
        var tokenData = new CreateTokenData();
        try {
            tokenData.loadDefaultPayloadData(new UserClaim(username))
                    .setAlgorithm(JWSAlgorithm.HS256)
                    .setSigner(new MACSigner(retrieveKey.getCurrentKey()));
        } catch (KeyLengthException e) {
            throw new RuntimeException("signer could not be initialized", e);
        }
        return tokenData;
    }

}
