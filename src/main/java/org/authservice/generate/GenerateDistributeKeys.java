package org.authservice.generate;

import org.authservice.generate.keys.HmacKey;
import org.authservice.generate.keys.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import javax.crypto.SecretKey;

@Component
class GenerateDistributeKeys {
    private final StoreKeys storeKeys;
    private final Key key;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private GenerateDistributeKeys(StoreKeys storeKeys, HmacKey key) {
        this.storeKeys = storeKeys;
        this.key = key;
    }

    //executes at 3am everyday
    @Scheduled(cron = "0 0 5 * * ?")
    private void generateAndDistributeNewKeys() {
        var secretKey = generateKey();
        saveSecretKey(secretKey);
    }

    private void saveSecretKey(SecretKey secretKey) {
        storeKeys.saveSecretKey(secretKey);
    }

    public SecretKey generateKey() {
        return key.generateKey(HmacKey.HMACSHA256);
    }

}
