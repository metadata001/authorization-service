package org.authservice.common.exceptions.handler;

import org.authservice.common.exceptions.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import static org.authservice.common.helper.ErrorValidationKeys.getTargets;
import java.util.*;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControllerExceptionHandler {
    public static final String ERRORS = "errors";
    public static final String UNDEFINED = "undefined";

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleExceptions(Exception ex) {
        var error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
        return new ResponseEntity<>(error, error.getStatus());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity getValidationError(MethodArgumentNotValidException exception) {
        var errors = getErrors(exception);
        var map = mappedErrors(errors, getTargets());
        return new ResponseEntity<>(map, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * @param errors represents a list of string validation errors
     * @param errorCodes a map of string error codes; ex. {password : {"password invalid"}}
     * @return Map; ex. {errors: {password: {"password is invalid"}}}
     *<br><br>
     * This method iterates over a map of pre-defined error codes which is compared against a list
     * of error strings. An error string is added to the map if the error code is contained in the string.
     * If there are still error elements in the errors list after iteration, remaining elements are mapped
     * as undefined errors. Map entries are then removed if they are empty after iteration. It is best practice
     * to avoid unmapped errors. Errors should be mapped in the ErrorValidationKeys.class.
     * <br><br>
     * To increase performance, each time an error string is added to the map,it is taken out of the list
     * and the index is decremented for slippage, so there are fewer error strings to iterate over each cycle.
     * <br><br>
     * The completed map is then put into another map with an "error" key for consumption. see @return
     * annotation for return structure.
     */
    private Map mappedErrors(List<String> errors, Map<String, Set<String>> errorCodes) {
        var errorsMap = new HashMap<>();
        for(Map.Entry errorCode : errorCodes.entrySet()) {
            for(int i = 0; i < errors.size(); i++) {
                if(errors.get(i).contains(errorCode.getKey().toString())) {
                    errorCodes.get(errorCode.getKey().toString()).add(errors.get(i));
                    errors.remove(errors.get(i));
                    --i;
                }
            }
        }
        errorCodes.entrySet().removeIf(entry -> entry.getValue().isEmpty());
        if(!errors.isEmpty()) {
            errorCodes.put(UNDEFINED, new HashSet<>(errors));
        }
        errorsMap.put(ERRORS, errorCodes);
        return errorsMap;
    }

    /**
     * @param exception takes a class that implements BindException interface
     * @return ArrayList - returns an arrayList of string validation errors;
     */
    private List<String> getErrors(BindException exception) {
        return exception.getFieldErrors().stream()
                .map(FieldError::getDefaultMessage)
                .collect(Collectors.toCollection(ArrayList::new));
    }

}