package org.authservice.login.service;

import org.authservice.login.model.DetailModel;
import org.authservice.login.repository.CredentialsRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import static org.authservice.login.service.query.CredentialsQuery.getByUserNameQuery;

@Service
public class UserDetailService implements UserDetailsService {
    private final CredentialsRepositoryImpl credentialsRepository;

    @Autowired
    public UserDetailService(CredentialsRepositoryImpl credentialsRepository) {
        this.credentialsRepository = credentialsRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var query = getByUserNameQuery(username);
        var credentials = credentialsRepository.findOne(query).orElseThrow(() ->
                new UsernameNotFoundException("username was not found")
        );
        return new DetailModel(credentials);
    }

}
