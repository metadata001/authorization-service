package org.authservice.jwt.model;

import org.springframework.data.mongodb.core.mapping.Document;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Document(collection = "compromised")
public class CompromisedUserDetails {
    private static final String TIME_ZONE = "America/New_York";
    private String whenCompromised;
    private String token;

    public String getWhenCompromised() {
        return this.whenCompromised;
    }

    public CompromisedUserDetails setWhenCompromised() {
        var zoneId = ZoneId.of(TIME_ZONE);
        var zonedTime = ZonedDateTime.now();
        var zonedDateTime = zonedTime.withZoneSameInstant(zoneId);
        whenCompromised = zonedDateTime.toLocalDateTime().toString();
        return this;
    }

    public String getToken() {
        return token;
    }

    public CompromisedUserDetails setToken(String token) {
        this.token = token;
        return this;
    }

}
