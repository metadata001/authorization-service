package org.authservice.jwt.model;

public class IssueTokenRequest {
    private String username;

    public String getUsername() {
        return username;
    }

    public IssueTokenRequest setUsername(String username) {
        this.username = username;
        return this;
    }

}
