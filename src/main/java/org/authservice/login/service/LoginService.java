package org.authservice.login.service;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import jakarta.validation.Valid;
import org.authservice.jwt.TokenValidationService;
import org.authservice.login.dto.RemoveUserCredentialsRequest;
import org.authservice.login.dto.UpdateUserNameRequest;
import org.authservice.login.dto.UpdateUserPasswordRequest;
import org.authservice.login.dto.UserCredentialsRequest;
import org.authservice.login.mapper.CredentialsMapper;
import org.authservice.login.model.DetailModel;
import org.authservice.login.model.UserCredentials;
import org.authservice.login.repository.CredentialsRepositoryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import static org.authservice.login.service.query.CredentialsQuery.getByUserNameQuery;
import static org.authservice.login.service.query.CredentialsUpdate.getUserCredentialsUpdate;

@Service
public class LoginService {
    private final CredentialsRepositoryImpl repository;
    private final CredentialsMapper mapper;
    private final BCryptPasswordEncoder encoder;
    private final AuthenticationManager authenticationManager;
    private final TokenValidationService tokenValidationService;
    private final Logger logger = LoggerFactory.getLogger(LoginService.class);

    @Autowired
    public LoginService(TokenValidationService tokenValidationService,
                        AuthenticationManager authenticationManager,
                        CredentialsRepositoryImpl repository,
                        CredentialsMapper mapper,
                        BCryptPasswordEncoder encoder) {
        this.tokenValidationService = tokenValidationService;
        this.authenticationManager = authenticationManager;
        this.repository = repository;
        this.mapper = mapper;
        this.encoder = encoder;
    }

    public UserCredentials signup(@Valid UserCredentialsRequest request) {
        var encoded = encoder.encode(request.getPassword());
        var query = getByUserNameQuery(request.getUsername());
        var credentials = mapper.map(request);
        credentials.setHashedPassword(encoded);
        credentials.setAuthorities(request.getAuthorities());

        repository.findOne(query).ifPresent(e -> {
            throw new RuntimeException("user already exists");
        });

        return repository.save(credentials).orElseGet(() -> {
            logger.warn("credentials could not be saved");
            return null;
        });
    }

    public DetailModel login(UserCredentialsRequest request) {
        var loginToken = new UsernamePasswordAuthenticationToken(
                request.getUsername(),
                request.getPassword(),
                request.getGrantedAuthorities()
        );

        var token = tokenValidationService.issueToken(request.getUsername());
        var detail = (DetailModel) authenticationManager.authenticate(loginToken).getPrincipal();
        detail.setToken(token.getTokenString());
        detail.setPassword(null);

        var message = String.format("user: %s, was successfully logged in", request.getUsername());
        logger.info(message);

        return detail;
    }


    public DetailModel updateUsername(@Valid UpdateUserNameRequest request) {
        var currentUsername = request.getCurrentUsername();
        var newUsername = request.getNewUserName();
        var query = getByUserNameQuery(currentUsername);
        var update = getUserCredentialsUpdate(request);

        var authToken = new UsernamePasswordAuthenticationToken(
                request.getCurrentUsername(),
                request.getPassword(),
                request.getGrantedAuthorities()
        );

        var verified =  authenticationManager.authenticate(authToken);
        var detail = (DetailModel) verified.getPrincipal();
        var token = tokenValidationService.issueToken(request.getNewUserName());
        detail.setToken(token.getTokenString());

        var updated = repository.updateFirst(query, update, UserCredentials.class);

        if(updated.getModifiedCount() > 0) {
            var message = String.format("user: %s, username was successfully updated to %s",
                    currentUsername, newUsername);
            logger.info(message);
            detail.setUsername(request.getNewUserName());
            detail.setPassword(null);

            SecurityContextHolder.clearContext();
            authToken = new UsernamePasswordAuthenticationToken(
                    request.getNewUserName(),
                    request.getPassword(),
                    request.getGrantedAuthorities()
            );
            authenticationManager.authenticate(authToken);

            return detail;

        }
        else {
            var message = String.format("user: %s, username was not updated", currentUsername);
            logger.warn(message);

        }

        return null;
    }

    public UpdateResult updatePassword(@Valid UpdateUserPasswordRequest request) {
        var username = request.getUsername();
        var query = getByUserNameQuery(username);
        var update = getUserCredentialsUpdate(request);

        if(request.getCurrentPassword().equals(request.getNewPassword())) {
            var message = String.format("password update for %s rejected, current and new password are the same", username);
            throw new RuntimeException(message);
        }
        var user = repository.findOne(query).orElseThrow(() ->
                new RuntimeException(String.format("user: %s, could not be found", username))
        );
        var authToken = new UsernamePasswordAuthenticationToken(
                request.getUsername(),
                request.getCurrentPassword(),
                user.getGrantedAuthorities()
        );

        authenticationManager.authenticate(authToken);
        var encoded = encoder.encode(request.getNewPassword());
        request.setNewPassword(encoded);
        var result = repository.updateFirst(query, update, UserCredentials.class);

        if(result.getModifiedCount() > 0) {
            SecurityContextHolder.clearContext();
            authToken = new UsernamePasswordAuthenticationToken(
                    request.getUsername(),
                    request.getNewPassword(),
                    user.getGrantedAuthorities()
            );
            authenticationManager.authenticate(authToken);

        }
        else {
            var message = String.format("user: %s, password was not updated", request.getUsername());
            logger.warn(message);

        }

        return result;
    }

    public DeleteResult removeCredentials(@Valid RemoveUserCredentialsRequest request) {
        var query = getByUserNameQuery(request.getUsername());

        var user = repository.findOne(query).orElseThrow(() ->
                new RuntimeException(String.format("user: %s, could not be found", request.getUsername()))
        );

        var authToken = new UsernamePasswordAuthenticationToken(
                request.getUsername(),
                request.getPassword(),
                user.getGrantedAuthorities()
        );
        authenticationManager.authenticate(authToken);
        var result = repository.remove(query, UserCredentials.class);

        if(result.getDeletedCount() > 0) {
            var message = String.format("user: %s, credentials were successfully deleted", request.getUsername());
            logger.info(message);
            SecurityContextHolder.clearContext();

        }
        else {
            var message = String.format("user: %s, credentials could not be deleted", request.getUsername());
            logger.warn(message);

        }

        return result;
    }

}
