package org.authservice.jwt.dto;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSSigner;

public class CreateTokenData {
    private JWTPayloadData payloadData;
    private JWSSigner signer;
    private JWSAlgorithm algorithm;

    public CreateTokenData() {}

    public CreateTokenData(JWTPayloadData data, JWSSigner signer, JWSAlgorithm algorithm) {
        this. payloadData = data;
        this.signer = signer;
        this.algorithm = algorithm;
    }

    public JWTPayloadData getPayloadData() {
        return payloadData;
    }

    public CreateTokenData setPayloadData(JWTPayloadData payloadData) {
        this.payloadData = payloadData;
        return this;
    }

    public JWSSigner getSigner() {
        return signer;
    }

    public CreateTokenData setSigner(JWSSigner signer) {
        this.signer = signer;
        return this;
    }

    public JWSAlgorithm getAlgorithm() {
        return algorithm;
    }

    public CreateTokenData setAlgorithm(JWSAlgorithm algorithm) {
        this.algorithm = algorithm;
        return this;
    }

    public CreateTokenData loadDefaultPayloadData(Object claim) {
        var data = new JWTPayloadData();
        data.setIssuer("Metadata")
                .setAudience("Registered User")
                .setIssued()
                .setExpiry(7)
                .setSubject("Access Token")
                .setClaim(claim);
        this.payloadData = data;
        return this;
    }

}
