package org.authservice.config;

import io.micrometer.common.util.StringUtils;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletResponseWrapper;
import org.authservice.jwt.TokenValidationService;
import org.authservice.login.model.DetailModel;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;
import java.util.Objects;

@WebFilter(urlPatterns = "/**", description = "jwt filter for registered endpoints")
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer ";
    private static final String TOKEN_HEADER = "JWT_TOKEN";
    private final AuthenticationManager authenticationManager;
    private final TokenValidationService tokenValidationService;

    @Autowired
    public JwtAuthenticationFilter(TokenValidationService tokenValidationService,
                                   AuthenticationManager authenticationManager) {
        this.tokenValidationService = tokenValidationService;
        this.authenticationManager = authenticationManager;
    }

    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request,
                                    @NotNull HttpServletResponse response,
                                    @NotNull FilterChain filterChain
    ) throws ServletException, IOException {
        var requestWrapper = new HttpServletRequestWrapper(request);
        var responseWrapper = new HttpServletResponseWrapper(response);
        final String authHeader = requestWrapper.getHeader(AUTHORIZATION);

        if (StringUtils.isEmpty(authHeader) || !authHeader.startsWith(BEARER)) {
            filterChain.doFilter(request, response);
            return;

        }

        var context = (DetailModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var tokenString = authHeader.substring(7);
        var username = tokenValidationService.extractUsername(tokenString);
        var token = new JwtAuthToken(username, tokenString);
        Authentication auth = null;

        if(context.getUsername().equals(username)) {
            try {
                auth = authenticationManager.authenticate(token);

                var authorities = auth.getAuthorities().stream().toList();

                token = new JwtAuthToken(Objects.requireNonNull(auth).getPrincipal().toString(),
                        (String) auth.getCredentials(), authorities);
                token.setAuthenticated(auth.isAuthenticated());
                token.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                responseWrapper.setHeader(TOKEN_HEADER, token.getCredentials());

            } catch (RuntimeException e) {
                var message = String.format("%s was most likely not a valid username, " +
                        "request could not be processed", username);
                logger.warn(message, e);

            } finally {
                filterChain.doFilter(requestWrapper, responseWrapper);

            }

        }
        else {
            throw new RuntimeException("context not present or invalid");

        }

    }
}
