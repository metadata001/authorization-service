package org.authservice.generate.keys;

import com.nimbusds.jose.JWSAlgorithm;
import org.authservice.common.exceptions.InvalidAlgorithmException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;

@Component
public class HmacKey extends Key {
    public static final String HMACSHA256 = "HmacSHA256";
    public static final String HMACSHA384 = "HmacSHA384";
    public static final String HMACSHA512 = "HmacSHA512";
    private static final Logger logger = LoggerFactory.getLogger(HmacKey.class);

    @Override
    public SecretKey generateKey(String algorithm) {
        try {
            var keyGen = KeyGenerator.getInstance(algorithm);
            keyGen.init(new SecureRandom());
            return keyGen.generateKey();

        } catch (NoSuchAlgorithmException exception) {
            var message = "Key pair was not successfully generated";
            logger.warn(exception.getMessage());
            logger.warn(message);

        }

        return null;
    }

    @Override
    public JWSAlgorithm getAlgorithm(String algorithm) {
        switch (algorithm) {
            case HMACSHA256 -> {
                return JWSAlgorithm.HS256;
            }
            case HMACSHA384 -> {
                return JWSAlgorithm.HS384;
            }
            case HMACSHA512 -> {
                return JWSAlgorithm.HS512;
            }
            default -> {
                var message = String.format("Invalid HMAC algorithm, valid algorithms, %s, %s, %s",
                        HMACSHA256, HMACSHA384, HMACSHA512);
                throw new InvalidAlgorithmException(message);
            }
        }
    }

    @Override
    public List<String> getApprovedAlgorithms() {
        return List.of(HMACSHA256, HMACSHA384, HMACSHA512);
    }

    @Override
    public List<JWSAlgorithm> getApprovedJWSAlgorithms() {
        return getApprovedAlgorithms().stream()
                .map(this::getAlgorithm)
                .toList();
    }

}
