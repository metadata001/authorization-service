package org.authservice.login.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import javax.persistence.GeneratedValue;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import static java.util.stream.Collectors.toCollection;

@Document(collection = "authentication")
public class UserCredentials {
    @Id
    @GeneratedValue
    private String id;
    @Field(name = "username")
    private String username;
    @Field(name = "password")
    private String hashedPassword;
    @Field(name = "authority")
    private List<String> authorities;

    public UserCredentials() {}

    public UserCredentials(String username, String hashedPassword,
                           List<String> authorities) {
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.authorities = authorities;
    }

    public String getId() {
        return id;
    }

    public UserCredentials setId(String id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserCredentials setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public UserCredentials setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
        return this;
    }

    public List<String> getAuthorities() {
        return Objects.requireNonNullElse(this.authorities, List.of());
    }

    public List<SimpleGrantedAuthority> getGrantedAuthorities() {
        return checkAuthorities(this.authorities);
    }

    public UserCredentials setAuthorities(List<String> authorities) {
        this.authorities = Objects.requireNonNullElse(authorities, List.of());
        return this;
    }

    private List<SimpleGrantedAuthority> checkAuthorities(List<String> authorities) {
        if(authorities != null && !authorities.isEmpty()) {
            return authorities.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(toCollection(ArrayList<SimpleGrantedAuthority>::new));

        }
        else {
            return new ArrayList<>();

        }
    }

}
