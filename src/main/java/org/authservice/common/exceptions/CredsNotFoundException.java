package org.authservice.common.exceptions;

public class CredsNotFoundException extends RuntimeException {

    public CredsNotFoundException(String message) {
        super(message, new RuntimeException());
    }

}
