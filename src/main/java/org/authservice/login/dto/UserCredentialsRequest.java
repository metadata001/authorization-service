package org.authservice.login.dto;


import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import org.authservice.common.helper.Regex;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import java.util.List;
import java.util.Objects;
import static jakarta.validation.constraints.Pattern.Flag.DOTALL;

public class UserCredentialsRequest {
        @NotBlank(message = "username is required")
        @Size(min = 6, message = "username must have at least 6 characters")
        private String username;
        @NotBlank(message = "password is required")
        @Size(min = 8, max = 12, message = "password must be be between 8 - 12 characters")
        @Pattern(regexp = Regex.LOWERCASE, flags = DOTALL, message = "password must contain at least one lower case letter")
        @Pattern(regexp = Regex.UPPERCASE, flags = DOTALL, message = "password must contain at least one upper case letter")
        @Pattern(regexp = Regex.DIGIT, flags = DOTALL, message = "password must contain at least one digit")
        @Pattern(regexp = Regex.SPECIAL_CHAR, flags = DOTALL, message = "password must contain at least one of these special characters [@#$&!+=-]")
        private String password;
        private List<String> authorities;

        public String getUsername() {
                return username;
        }

        public UserCredentialsRequest setUsername(String username) {
                this.username = username;
                return this;
        }

        public String getPassword() {
                return password;
        }

        public UserCredentialsRequest setPassword(String password) {
                this.password = password;
                return this;
        }

        public UserCredentialsRequest setAuthorities(List<String> authorities) {
                this.authorities = Objects.requireNonNullElse(authorities, List.of());
                return this;
        }

        public List<String> getAuthorities() {
                return Objects.requireNonNullElse(this.authorities, List.of());
        }

        public List<SimpleGrantedAuthority> getGrantedAuthorities() {
                return convertAuthorities(this.authorities);
        }

        private List<SimpleGrantedAuthority> convertAuthorities(List<String> authorities) {
                if(authorities != null && !authorities.isEmpty()) {
                        return authorities.stream()
                                .map(SimpleGrantedAuthority::new)
                                .toList();
                }
                else {
                        return List.of();
                }
        }

}
