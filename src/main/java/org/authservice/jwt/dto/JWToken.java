package org.authservice.jwt.dto;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import org.authservice.common.exceptions.PayloadDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.crypto.SecretKey;
import java.text.ParseException;

public class JWToken {
    private JWSObject jwt;
    private JWSHeader header;
    private JWTPayloadData payloadData;
    private Payload payload;
    private JWSSigner signer;
    private final Logger logger = LoggerFactory.getLogger(JWToken.class);

    public JWSHeader getHeader() {
        return header;
    }

    public JWToken setHeader(JWSAlgorithm algorithm) {
        this.header = new JWSHeader(algorithm);
        return this;
    }

    public JWTPayloadData getPayloadData() {
       return this.payloadData;
    }

    public Payload getPayload() {
        return payload;
    }

    public JWToken setPayload() {
        if(this.payloadData != null) {
            this.payload = new Payload(this.payloadData.getPayload().toString());
        }
        else {
            var message = "jwt payload data is not set";
            throw new PayloadDataException(message);
        }
        return this;
    }

    public JWToken setPayloadData(JWTPayloadData payloadData) {
        this.payloadData = payloadData;
        return this;
    }

    public JWSSigner getJwsSigner() {
        return signer;
    }

    public JWToken setMacSigner(SecretKey secretKey) {
        try {
            this.signer = new MACSigner(secretKey);

        } catch (KeyLengthException e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    public JWSObject getToken() {
        if(jwt != null) {
            return jwt;
        }
        else {
            var message = "cannot get null token";
            logger.warn(message);
            return null;
        }
    }

    public JWToken createToken() {
        if(tokenSet()) {
            try {
                var encoded_header = header.toBase64URL();
                var encoded_payload = payload.toBase64URL();
                var signature = signer.sign(header, payload.toBytes());
                jwt = new JWSObject(encoded_header, encoded_payload, signature);

            } catch (ParseException | JOSEException e) {
                var message = "failed to create json web object";
                logger.warn(message);
                logger.warn(e.toString());
            }
        }
        return this;
    }

    public JWToken signToken() {
        if(tokenSet()) {
            try {
                this.signer.sign(this.header, this.payload.toBytes());

            } catch (JOSEException e) {
                var message = "token could not be signed due to error";
                throw new RuntimeException(message);
            }
        }
        else {
            var message = "cannot sign token because the token is not set";
            throw new RuntimeException(message);
        }
        return this;
    }

    public String getTokenString() {
        return this.getToken().getHeader().toBase64URL() + "." +
                this.getToken().getPayload().toBase64URL() + "." +
                this.getToken().getSignature();
    }

    private boolean tokenSet() {
        return header != null && payload != null && signer != null;
    }

}
