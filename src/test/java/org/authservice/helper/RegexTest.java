package org.authservice.helper;

import org.authservice.common.helper.Regex;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RegexTest {
    @Test
    void emailPattern_shouldMatch() {
        var email = "joebob@domain.com";
        var matched = isValidMatcher(email, Regex.EMAIL);
        Assertions.assertTrue(matched);
    }

    @Test
    void lowerCasePattern_shouldMatch() {
        var lowercase = "lowercase";
        var matched = isValidMatcher(lowercase, Regex.LOWERCASE);
        Assertions.assertTrue(matched);
    }

    @Test
    void upperCasePattern_shouldMatch() {
        var uppercase = "UPPERCASE";
        var matched = isValidMatcher(uppercase, Regex.UPPERCASE);
        Assertions.assertTrue(matched);
    }

    @Test
    void digitPattern_shouldMatch() {
        var digit = "1234567890";
        var matched = isValidMatcher(digit, Regex.DIGIT);
        Assertions.assertTrue(matched);
    }

    @Test
    void specialCharPattern_shouldMatch() {
        var digit = "@#$&!+=-";
        var matched = isValidMatcher(digit, Regex.SPECIAL_CHAR);
        Assertions.assertTrue(matched);
    }

    public boolean isValidMatcher(String matcher, String pattern) {
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(pattern);
        java.util.regex.Matcher m = p.matcher(matcher);
        return m.matches();
    }
}
