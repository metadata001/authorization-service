package org.authservice.jwt.model;

public class ValidatedTokenResponse {
    private boolean valid;
    private boolean expired;
    private String username;
    private String tokenString;

    public boolean isValid() {
        return valid;
    }

    public ValidatedTokenResponse setValid(boolean valid) {
        this.valid = valid;
        return this;
    }

    public boolean isExpired() {
        return expired;
    }

    public ValidatedTokenResponse setExpired(boolean expired) {
        this.expired = expired;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public ValidatedTokenResponse setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getTokenString() {
        return tokenString;
    }

    public ValidatedTokenResponse setTokenString(String tokenString) {
        this.tokenString = tokenString;
        return this;
    }

}
