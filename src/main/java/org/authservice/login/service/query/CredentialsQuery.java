package org.authservice.login.service.query;

import org.springframework.data.mongodb.core.query.Query;
import static org.authservice.common.helper.ErrorValidationKeys.USERNAME;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

public class CredentialsQuery {

    public static Query getByUserNameQuery(String username) {
        return query(where(USERNAME).is(username));
    }

}
