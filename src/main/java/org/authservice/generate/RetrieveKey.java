package org.authservice.generate;

import org.authservice.generate.storage.SecretPhraseStore;
import org.authservice.generate.storage.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.crypto.SecretKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

@Component
public class RetrieveKey {
    private final Store storage;
    private static final Logger logger = LoggerFactory.getLogger(RetrieveKey.class);

    public RetrieveKey(SecretPhraseStore storage) {
        this.storage = storage;
    }

    public SecretKey getCurrentKey() {
        var file = new File(storage.getCurrentFilepath());
        return getSecretKey(file, storage.getCurrentAlias());
    }

    public SecretKey getExpiredKey() {
        var file = new File(storage.getExpiredFilepath());
        return getSecretKey(file, storage.getExpiredAlias());
    }

    private SecretKey getSecretKey(File file, String alias) {
        try(var input = new FileInputStream(file)) {
            var keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            if (file.length() > 0) {
                //loads chosen keystore, extracts key and saves it to expired keystore
                keyStore.load(input, storage.getPassword().toCharArray());
                return this.getSecretKey(keyStore, alias);

            }

            return null;

        } catch (KeyStoreException | IOException | CertificateException | NoSuchAlgorithmException e) {
            var message = "keystore could not be loaded";
            logger.warn(message);
            throw new RuntimeException(e);

        }

    }

    private SecretKey getSecretKey(KeyStore keyStore, String alias) {
        try {
            KeyStore.ProtectionParameter param = new KeyStore.PasswordProtection(storage.getPassword().toCharArray());
            var entry = (KeyStore.SecretKeyEntry) keyStore.getEntry(alias, param);
            return entry.getSecretKey();

        } catch (NoSuchAlgorithmException | UnrecoverableEntryException | KeyStoreException e) {
            logger.warn("failed to recover key from keystore");
            logger.warn(e.toString());

        }

        return null;
    }

}
