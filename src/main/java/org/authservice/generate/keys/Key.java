package org.authservice.generate.keys;

import com.nimbusds.jose.JWSAlgorithm;
import javax.crypto.SecretKey;
import java.util.List;

public abstract class Key {

    public abstract SecretKey generateKey(String algorithm);

    public abstract JWSAlgorithm getAlgorithm(String algorithm);

    public abstract List<String> getApprovedAlgorithms();

    public abstract List<JWSAlgorithm> getApprovedJWSAlgorithms();

}
