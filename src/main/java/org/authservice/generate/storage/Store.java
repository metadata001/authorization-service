package org.authservice.generate.storage;

public interface Store {

    String getExpiredFilepath();

    String getCurrentFilepath();

    String getCurrentAlias();

    String getExpiredAlias();

    String getPassword();

}
